import toml
from pathlib import Path


class Server:
    def __init__(self, cfg):
        self.host = cfg['host']
        self.port = cfg['port']


class AzureClient:
    def __init__(self, cfg):
        self.client_id = cfg['client_id']
        self.client_secret = cfg['client_secret']
        self.authority = cfg['authority']
        self.redirect_path = cfg['redirect_path']
        self.scope = cfg['scope']


class Api:
    def __init__(self, cfg):
        self.specification_dir = cfg['specification_dir']
        self.openapi_file = cfg['openapi_file']


class BackendHost:
    def __init__(self, cfg):
        self.host = cfg['host']
        self.specification_dir = cfg['specification_dir']
        self.openapi_file = cfg['openapi_file']

class Config:
    def __init__(self, cfg):
        self.server = Server(cfg['server'])
        self.azure_client = AzureClient(cfg['azure_client'])
        self.api = Api(cfg['api'])
        self.backend_host = BackendHost(cfg['backend_host'])


config = Config(toml.load(Path(__file__).parent / 'config.toml'))
