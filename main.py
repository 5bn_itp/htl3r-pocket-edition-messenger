from azure_ad_verify_token import verify_jwt
from flask import Flask, request
from flask_cors import CORS
from flask_socketio import SocketIO, send, emit, join_room, disconnect

from config import config
from openapi_client import *

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
cors = CORS(app, resources={r"*": {"origins": "*/*"}})
socketio = SocketIO(app, cors_allowed_origins="*")
api_config = Configuration(
    host=config.backend_host.host
)


def chat_id_from_data(data):
    return str(data['chat_id'])


def user_id_from_data(data):
    return data['user_id']


def auth_api_client(token):
    return ApiClient(api_config, header_name='Authorization', header_value=f'Bearer {token}')


clients = {}


def verify_token():
    try:
        token = request.headers.get('Authorization').split(' ')[1]
        token_info = verify_jwt(
            token=token,
            valid_audiences=[config.azure_client.client_id],
            issuer=f'{config.azure_client.authority}/v2.0',
            jwks_uri=f'{config.azure_client.authority}/discovery/v2.0/keys',
            verify=True
        )
        return token, token_info
    except Exception as e:
        print(e)
        return None, None


@socketio.on('connect', namespace='/chat')
def on_connect():
    token, token_info = verify_token()
    if not token_info:
        print('not authenticated')
        disconnect()
        return
    clients[request.sid] = token, token_info


@socketio.on('join', namespace='/chat')
def on_join(data):
    token, token_info = clients[request.sid]
    name = token_info['name']
    chat_id = chat_id_from_data(data)
    join_room(chat_id)
    print(f'{request.sid}: {name} joined {chat_id}')
    emit('joined', {"msg": f'{name} joined {chat_id}'}, to=chat_id, include_self=False)


@socketio.on('send', namespace='/chat')
def on_send(data):
    token, token_info = clients[request.sid]
    with auth_api_client(token) as api:
        instance = ChatsApi(api)
        user_api = UsersApi(api)
        chat_id = chat_id_from_data(data)
        user = user_api.api_get_user_by_oid(token_info['oid'])
        msg = data['msg']
        send({"msg": msg, "from": {
            "role_id": user.role_id,
            "user_email": user.user_email,
            "user_id": user.user_id,
            "user_name": user.user_name,
            "user_oid": user.user_oid
        }}, to=chat_id)

        instance.api_add_chat_message(chat_id=int(chat_id), create_message=CreateMessage(
            _from=user.user_id,
            content=msg
        ))


if __name__ == '__main__':
    print("Starting server at " + config.server.host + ":" + str(config.server.port))
    socketio.run(app, host=config.server.host, port=config.server.port)
